FROM node:17.2-alpine as build

WORKDIR /build
COPY . .

RUN npm install
RUN npx nx run-many --target=build --all
RUN rm -rf node_modules && npm install --production

# ===================================

FROM node:17.2-alpine as api-service

WORKDIR /app

COPY --from=build /build/dist/apps/api-service .
COPY --from=build /build/node_modules node_modules/
COPY --from=build /build/package.json .

CMD npm run start:prod

# =====================================

FROM node:17.2-alpine as stock-service

WORKDIR /app

COPY --from=build /build/dist/apps/stock-service .
COPY --from=build /build/node_modules node_modules/
COPY --from=build /build/package.json .

CMD npm run start:prod
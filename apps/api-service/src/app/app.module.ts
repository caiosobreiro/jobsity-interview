import { Module } from '@nestjs/common'

import { AppController } from './app.controller'
import { AuthController } from './auth/auth.controller'
import { StockController } from './stock/stock.controller'
import { RedisService } from './redis/redis.service'
import { AuthService } from './auth/auth.service'
import { StockService } from './stock/stock.service'

@Module({
  imports: [],
  controllers: [AppController, AuthController, StockController],
  providers: [RedisService, AuthService, StockService],
})
export class AppModule {}

import { Injectable, Logger } from '@nestjs/common'
import { createClient, RedisClientType } from 'redis'

@Injectable()
export class RedisService {

    client: RedisClientType<any>
    connected: boolean

    constructor () {
        const options = {
            url: process.env.REDIS_URL || 'redis://localhost:6379'
        }
        this.client = createClient(options)
        this.client.on('error', (err: Error) => {
            Logger.log('Redis Client Error', err)
            this.connected = false
        })
    }

    async connectIfNeeded () {
        if (!this.connected) {
            await this.client.connect()
            this.connected = true
        }
    }

    async get(key: string) {
        await this.connectIfNeeded()
        const data = await this.client.get(key)
        return JSON.parse(data)
    }

    async set (key: string, data: any) {
        await this.connectIfNeeded()
        return this.client.set(key, JSON.stringify(data))
    }

    async hSet (key: string, field: string, value: any) {
        await this.connectIfNeeded()
        return this.client.hSet(key, field, JSON.stringify(value))
    }

    async hmGet (key: string, field: string) {
        await this.connectIfNeeded()
        return this.client.hmGet(key, field)
    }

    async hGetAll (key: string) {
        await this.connectIfNeeded()
        return this.client.hGetAll(key)
    }

}

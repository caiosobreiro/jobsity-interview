import { HttpException, Injectable, Logger } from '@nestjs/common'
import axios from 'axios'
import { RedisService } from '../redis/redis.service'

@Injectable()
export class StockService {

    constructor (private redisService: RedisService) { }

    async getStockQuote(code: string) {
        const stockServiceUrl = process.env.STOCK_SERVICE_URL
        if (!stockServiceUrl) {
            const errorMessage = 'Failed to call stock-service. Configure the STOCK_SERVICE_URL env variable'
            Logger.error(errorMessage)
            throw new HttpException(errorMessage, 400)
        }

        const url = new URL(`/api/stock/${code}`, stockServiceUrl)
        const { data } = await axios.get(url.href)
        return data
    }

    /**
     * Save query history
     */
    async saveQuoteToDB(quote: Quote, username: string) {
        await this.redisService.hSet(`quotes:${username}`, Date.now().toString(), quote)

        const hitCount = Number(await this.redisService.hmGet('stats', quote.Symbol)) || 0
        await this.redisService.hSet(`stats`, quote.Symbol, hitCount + 1)
    }

    async fetchUserHistory (username: string) {
        const key = `quotes:${username}`
        const data = await this.redisService.hGetAll(key)
        return Object.keys(data).map(field => {
            const obj = JSON.parse(data[field])
            obj.QueryDate = (new Date(Number(field))).toString()
            return obj
        })
    }

    async getStats () {
        const stats = await this.redisService.hGetAll('stats')
        const filtered = Object
            .entries(stats)
            .sort((a, b) => Number(b[1]) - Number(a[1]))
        filtered.splice(5)
        return Object.fromEntries(filtered)
    }
}

export interface Quote {
    Symbol: string,
    Date: string,
    Time: string,
    Open: string,
    High: string,
    Low: string,
    Close: string,
    Volume: string,
    Name: string
}

import { Controller, Get, Headers, Param, UseGuards, HttpException } from '@nestjs/common'
import { ApiBearerAuth } from '@nestjs/swagger'
import { AuthGuard } from '../auth/auth.guard'
import { StockService } from './stock.service'

@Controller('stock')
@ApiBearerAuth()
export class StockController {

    constructor (private stockService: StockService) { }

    @Get('code/:code')
    @UseGuards(AuthGuard)
    async getStockQuote(@Param('code') code: string, @Headers() headers) {
        const quote = await this.stockService.getStockQuote(code)
        // save quote to db
        await this.stockService.saveQuoteToDB(quote, headers.tokenData.username)
        return quote
    }

    @Get('history')
    @UseGuards(AuthGuard)
    async getQueryHistory( @Headers() headers ) {
        // fetch user history from db
        return this.stockService.fetchUserHistory(headers.tokenData.username)
    }

    @Get('stats')
    @UseGuards(AuthGuard)
    async getStats( @Headers() headers ) {
        if (headers.tokenData.permissionLevel !== 'admin') {
            throw new HttpException('User permissions are not sufficient', 401)
        }

        // return stock stats
        return this.stockService.getStats()
    }
}

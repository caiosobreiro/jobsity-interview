import { Test, TestingModule } from '@nestjs/testing'
import { RedisService } from '../redis/redis.service'
import { StockService } from './stock.service'

describe('StockService', () => {
  let service: StockService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StockService, RedisService],
    }).compile()

    service = module.get<StockService>(StockService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})

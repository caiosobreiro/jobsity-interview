import { Test, TestingModule } from '@nestjs/testing'
import { AuthService } from '../auth/auth.service'
import { RedisService } from '../redis/redis.service'
import { StockController } from './stock.controller'
import { StockService } from './stock.service'

describe('StockController', () => {
  let controller: StockController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockController],
      providers: [AuthService, RedisService, StockService]
    }).compile()

    controller = module.get<StockController>(StockController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

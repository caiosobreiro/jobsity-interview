import { HttpException, Injectable } from '@nestjs/common'
import { RedisService } from '../redis/redis.service'
import * as crypto from 'crypto'
import * as jwt from 'jsonwebtoken'

@Injectable()
export class AuthService {

    JWT_PASSWORD = 'a1e9e18ae630f40f466019e80748f965'

    constructor (private redisService: RedisService) { }

    async authorize(user: string, password: string) {
        const key = `users:${user}`
        const userData = await this.redisService.get(key)

        if (!userData) {
            throw new HttpException('User atuthentication failed', 401)
        }

        if (userData.password !== this.generateSHA256Hash(password)) {
            throw new HttpException('User atuthentication failed', 401)
        }

        // generate token with userdata
        const tokenData = {
            username: user,
            permissionLevel: userData.permissionLevel
        }

        const token = this.createToken(tokenData)
        return { token }
    }

    async createUser(user: string, permission: string) {
        // generate random password
        const length = 32
        const password = crypto
            .randomBytes(length)
            .toString('base64')
            .slice(0, length)

        // save user data to db
        const userData = {
            password: this.generateSHA256Hash(password),
            permissionLevel: permission
        }

        const key = `users:${user}`
        await this.redisService.set(key, userData)

        //return user password
        return { password }
    }

    generateSHA256Hash (input: string) {
        return crypto.createHash('sha256').update(input).digest('hex')
    }

    createToken(tokenData: { username: string }) {
        return jwt.sign(tokenData, this.JWT_PASSWORD)
    }

    validateToken (token: string) {
        return jwt.verify(token, this.JWT_PASSWORD)
    }
}

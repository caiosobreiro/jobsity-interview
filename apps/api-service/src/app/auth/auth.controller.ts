import { Body, Controller, HttpException, Post } from '@nestjs/common'
import { ApiBody, ApiProperty } from '@nestjs/swagger'
import { AuthService } from './auth.service'

export class AuthRequest {
    @ApiProperty()
    username: string

    @ApiProperty()
    password: string
}

export class RegisterRequest {
    @ApiProperty()
    username: string

    @ApiProperty()
    permission: 'user' | 'admin'
}

@Controller('auth')
export class AuthController {

    constructor (private authService: AuthService) { }

    /**
     * Validate user credentials and return a token
     */
    @Post()
    @ApiBody({ type: AuthRequest })
    async Authorize(@Body('username') user: string, @Body('password') password: string) {
        if (!user || !password) {
            throw new HttpException('Username and password are required', 400)
        }

        return this.authService.authorize(user, password)
    }

    /**
     * Create new user and save to DB
     */
    @Post('register')
    @ApiBody({ type: RegisterRequest })
    RegisterUser(@Body('username') user: string, @Body('permission') permission: string) {
        if (!user) {
            throw new HttpException('username is required', 400)
        }

        return this.authService.createUser(user, permission || 'user')
    }
}

import { RedisService } from '../redis/redis.service'
import { AuthGuard } from './auth.guard'
import { AuthService } from './auth.service'

describe('AuthGuard', () => {

  const redisService = new RedisService()
  const authService = new AuthService(redisService)
  it('should be defined', () => {
    expect(new AuthGuard(authService)).toBeDefined()
  })
})

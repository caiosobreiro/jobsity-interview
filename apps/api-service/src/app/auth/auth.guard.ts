import { CanActivate, ExecutionContext, HttpException, Injectable } from '@nestjs/common'
import { Observable } from 'rxjs'
import { AuthService } from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {

  constructor (private authService: AuthService) { }

  canActivate( context: ExecutionContext ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest()

    const auth = request.headers.authorization
    if (!auth) {
      throw new HttpException('Token is required', 401)
    }

    // decrypt JWT token
    const token = auth.match(/Bearer (.*)/).at(1)
    try {
      const userData = this.authService.validateToken(token)
      request.headers.tokenData = userData
      return true
    } catch (err) {
      throw new HttpException(err.message, 401)
    }
  }
}

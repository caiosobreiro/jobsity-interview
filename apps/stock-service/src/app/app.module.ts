import { Module } from '@nestjs/common'

import { AppController } from './app.controller'
import { StockController } from './stock/stock.controller'
import { CsvParserService } from './csv-parser/csv-parser.service'

@Module({
  imports: [],
  controllers: [AppController, StockController],
  providers: [CsvParserService],
})
export class AppModule {}

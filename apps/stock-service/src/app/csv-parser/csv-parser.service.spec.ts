import { Test, TestingModule } from '@nestjs/testing'
import { CsvParserService } from './csv-parser.service'

describe('CsvParserService', () => {
  let service: CsvParserService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CsvParserService],
    }).compile()

    service = module.get<CsvParserService>(CsvParserService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('Should parse CSV correctly', () => {
    const csv = 'Symbol,Date,Time,Open,High,Low,Close,Volume,Name\r\nAAPL.US,2021-12-20,22:00:12,168.28,170.58,167.46,169.75,107499114,APPLE'
    const parsed = service.parseToObject(csv)
    expect(parsed).toBeDefined()
    expect(Object.keys(parsed)).toHaveLength(9)
    expect(parsed.Symbol).toEqual('AAPL.US')
  })

  it('Invalid CSV', () => {
    const csv = 'abc'
    try {
      service.parseToObject(csv)
      expect(1).toEqual(0)
    } catch(err) {
      expect(err).toBeDefined()
      expect(err.message).toEqual('Invalid CSV')
    }
  })
})

import { Injectable } from '@nestjs/common'

@Injectable()
export class CsvParserService {

    /**
     * @param csv The CSV string to parse
     * 
     * This parser assumes:
     *  - each line is separated by '\r\n'
     *  - The value separator is comma ','
     *  - The first line is the header
     */
    parseToObject (csv: string) {
        const lines = csv.split('\r\n')
        const valuesByLine = lines.map(l => l.split(','))

        if (lines.length !== 2) {
            throw new Error('Invalid CSV')
        }

        const output = {} as {[id: string]: string}
        const headerLine = valuesByLine[0]
        headerLine.forEach((header, index) => {
            output[header] = valuesByLine[1][index]
        })

        return output
    }
}

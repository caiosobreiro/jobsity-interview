import { Controller, Get, Param } from '@nestjs/common'
import axios from 'axios'
import { CsvParserService } from '../csv-parser/csv-parser.service'

@Controller('stock')
export class StockController {

    constructor (private csvParserService: CsvParserService) {}

    @Get('/:code')
    async getStockQuote(@Param('code') code: string) {
        const url = new URL(`/q/l/?s=${code}&f=sd2t2ohlcvn&h&e=csv`, 'https://stooq.com')
        const { data } = await axios.get(url.href)

        const parsed = this.csvParserService.parseToObject(data)
        return parsed
    }
}

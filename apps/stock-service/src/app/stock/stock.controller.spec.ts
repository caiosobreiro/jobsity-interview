import { Test, TestingModule } from '@nestjs/testing'
import { CsvParserService } from '../csv-parser/csv-parser.service'
import { StockController } from './stock.controller'

describe('StockController', () => {
  let controller: StockController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockController],
      providers: [CsvParserService]
    }).compile()

    controller = module.get<StockController>(StockController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

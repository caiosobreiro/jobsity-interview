# JobsityInterview

This project was generated using [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png" width="220"></p>

## Development server

Run `docker-compose up` for a dev server. Navigate to http://localhost:3333/ and http://localhost:3334/. The app will automatically reload if you change any of the source files.

## Build

Run `nx build api-service` or `nx build stock-service` to build the projects. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

For production-ready containers run `docker build --tag api-service --target api-service .` and `docker build --tag stock-service --target stock-service .`

## Running unit tests

Run `nx test api-service` and `nx test stock-service` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Understand the workspace

Run `nx dep-graph` to see a diagram of the dependencies of the projects.
